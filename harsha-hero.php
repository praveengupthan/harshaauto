<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">

        <!-- banner-->
        <div class="banner-groupitem">
            <img src="uploads/hero-banner.jpg" alt="" class="img-responsive">
        </div>
        <!--/ banner -->

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Harsha Hero</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="javascript:void(0)">Group Companies</a></li>
                                    <li class="active">Harsha Hero</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="post-media service-img">
                            <img src="uploads/service_01.jpg" alt="" class="img-responsive">
                        </div><!-- end post-media -->                                  
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12 m30">
                        <div class="section-title small-margin-title clearfix">
                            <h5>Welcome To Harsha Hero</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->            
                        <div class="service-text">                        
                            <p>The partnership which paved the way to establish a vast business empire, Sri Harsha Hero is the first channel partner venture of Harsha Automotive Group is one of the top selling authorized dealer point for Hero Motocorp in entire South India. The establishment is equipped with a spacious showroom of 4000 sft, modern work shop of 4000+ sft, more than 40 skilled & trained service experts / technicians and state-of-art equipment. With Quality and Customer Satisfaction becoming the guidelines for our business operations and complete focus on Manpower Development as the direction, Harsha Group grew exponentially in the competitive field of Automobile Industry to achieve a high level of success in a very short span of 14 years. </p> 
                        </div><!-- end service-text -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PARALLAX
        ********************************************** -->

        

        <!-- ******************************************
        SERVICES SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Full Range Of Services</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-truck-wheel"></i>
                            <h4>Engine & Transmission</h4>
                            <p>The clutch in a manual-shift motorcycle transmission is typically an arrangement of plates stacked in alternating fashion.</p>                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-painting-paint-roller"></i>
                            <h4>Fuel Efficiency</h4>
                            <p>The master cylinder/ reservoir is located in the right handlebar.The master cylinder/ reservoir is located behind right side cover next to the battery.</p>                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-technology-1"></i>
                            <h4>Wheels & Suspension</h4>
                            <p>Having a front suspension is common in most hybrids.But having a rear suspension is not recommended.</p>
                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-hose"></i>
                            <h4>Brakes</h4>
                            <p> Styling is a subjective matter and thus, every human has an individual set of parameters to define beauty. Here in 200cc segment.</p>
                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis">

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-malfunction-indicador"></i>
                            <h4>Bike Dimensions</h4>
                            <p>Hero’s first foray into the 200cc segment is this, the Xtreme 200R. It is Hero Motocorp’s first dip into the premium segment in a while.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-oil"></i>
                            <h4>Battery Saver</h4>
                            <p>An electric battery is a device consisting of one or more electrochemical cells with external connections provided to power electrical devices such as flashlights.</p>                            
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-disc-brake"></i>
                            <h4>Full Bike Service</h4>
                            <p>Our Full Service exceeds most manufacturers service schedules. If you book a full car service every year, you give yourself the best chance of totally trouble-free motoring...</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-timing-belt"></i>
                            <h4>Major Bike Service</h4>
                            <p> This extremely comprehensive service should be undertaken every two years, or snt to make sure all components are working safely and effectively,</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->

             <!-- banner section -->
        <div class="banner-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="uploads/herobanner01.jpg" alt="" class="img-responsive">
                    </div>
                    <!-- /col -->
                      <!-- col -->
                      <div class="col-lg-6">
                        <img src="uploads/herobanner02.jpg" alt="" class="img-responsive">
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--banner section -->

        </div><!-- end section -->     
       



        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section db">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-left">
                            <h4>Harsha Hero Branches</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">                   
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Visakhapatnam</h5>
                            
                            <p>Swarna Bharathi Indoor Stadium, New resapuvanipalem,Maddilapalem, Visakhapatnam- 530013, Andra Pradesh</p>
                            <p>Ph: 0891-2555125 (5 Lines), 9246619602/07</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Visakhapatnam</h5>
                            
                            <p>Butchirajupalem, Bhashyam School Opp. Baji Junction Main Road, Visakhapatnam- 530013, Andra Pradesh</p>
                           
                            <p>Ph: 0891-2742879/890, 9246618349, 9248749243/236</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->


                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        CALLBOX
        ********************************************** -->
        <div class="section bg callmewrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <h2><i class="flaticon-customer-service"></i> Please Call our Toll free number!</h2>
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <h2><i class="flaticon-phone-call"></i> 0891-2555125</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        BRANDS
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Hero Genuine Parts</h4>
                            <p>our Hero two-wheeler defines who you are. So, why not define your individuality even further with Hero Genuine Accessories. Fabricated with the highest standards, these accessories will enhance your style as well as provide best of aesthetics, quality and the unparalleled trust that comes with the Hero brand. So, get set and accessorize yourself with Hero Genuine Accessories.</p>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row clients">
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero01.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero02.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero03.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero04.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero05.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero06.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero07.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero08.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero09.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero10.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero11.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/hero/hero12.jpg" alt="" class="img-responsive">
                    </div>
                </div><!-- end carousel -->
            </div><!-- end container -->
        </div><!-- end section -->      


        

        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>