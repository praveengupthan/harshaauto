<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">

        <!-- banner-->
        <div class="banner-groupitem">
            <img src="uploads/volvo-bus-1.jpg" alt="" class="img-responsive">
        </div>
        <!--/ banner -->

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Harsha Volvo</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="javascript:void(0)">Group Companies</a></li>
                                    <li class="active">Harsha Volvo</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="post-media service-img">
                            <img src="uploads/volvo01.png" alt="" class="img-responsive">
                        </div><!-- end post-media -->                                  
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12 m30">
                        <div class="section-title small-margin-title clearfix">
                            <h5>Welcome To Harsha Volvo</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->            
                        <div class="service-text">                        
                            <p>The reputed Swedish brand for HUV’s & Commercial vehicles has chosen Harsha Automotive as their service partner in AP & Telangana states. The modern dynamic, integrated, high quality & fully equipped work shop at Hyderabad Harsha Volvo services the Volvo buses & trucks thereby fulfilling the after sale support to Volvo customer’s. Sree Harsha Automotive services (P) Ltd. an ISO 9001:2000 certified company, is an authorized service dealer for Volvo Trucks & Volvo Buses started in 2005 at Hyderabad. We started our foray into a highly customer centric division of Automobile industry with an ambition to provide the best services and make customer delighted.</p> 
                        </div><!-- end service-text -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->
        
        <!-- ******************************************
        SERVICES SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Technology</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-truck-wheel"></i>
                            <h4>Transmission</h4>
                            <p>Volvo I-Shift 12-speed automated gear-changing system with integrated retarder.

</p>                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-painting-paint-roller"></i>
                            <h4>Suspension</h4>
                            <p>Electronically controlled air suspension with kneeling function available on all variants.</p>                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-technology-1"></i>
                            <h4>Brakes</h4>
                            <p>Volvo electronically controlled disc brakes (EBS), Volvo Engine Brake (VEB) and Brake Blending function.</p>
                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                   
                </div><!-- end row -->

              

                <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-hose"></i>
                            <h4>Safety Features</h4>
                            <p> Knee Impact Protection, Front Impact Protection, Front Under run Protection System. Rear under run protection, side under run protection. </p>
                           
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-malfunction-indicador"></i>
                            <h4>Engine Range</h4>
                            <p>Volvo D11K 430 6cyl 10.8 litre Euro 6 engine rated at 430hp (12.3 m, 13 m) @1800 rpm with maximum torque of 2050Nm at 1000-1400rpm.</p>
                          
                        </div><!-- end service-hover -->
                    </div><!-- end col -->                 

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-disc-brake"></i>
                            <h4>Length Of A Coach Bus</h4>
                            <p>You should inquire about the amenities, which vary between tour bus companies. The most luxurious category is the Full Size Motor Coach.</p>
                            
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                  
                </div><!-- end row -->
            </div><!-- end container -->         

        </div><!-- end section -->    
       



        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section db">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-left">
                            <h4>Harsha Volvo Branches</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">                   
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Hyderabad</h5>
                            
                            <p>Volvo Buses & Trucks India, Survery No 345 part & 346/B-part,  Bachupally Village, Quthbullapur Mandal,
                                R.R District, Hyderabad – 500090.</p>
                            <p>Phone No.: +91 40 4020 4455,57 & +91 40 4021 582 </p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        CALLBOX
        ********************************************** -->
        <div class="section bg callmewrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <h2><i class="flaticon-customer-service"></i> Please Call our Toll free number!</h2>
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <h2><i class="flaticon-phone-call"></i> 0891-2742879/890</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-6">
                <img src="uploads/volvo04.jpg" alt="" class="img-responsive">
                    
                </div>
                <div class="col-lg-6">
                    <h2 class="h2">A GENUINE VOLVO QUALITY SERVICE</h2>
                    <p>Genuine Volvo Service is the best possible service for your bus. It’s performed by skilled Volvo mechanics at an authorised Volvo Bus & Coach workshop. Always using the latest tools and diagnostic equipment, and of course only fitting Genuine Volvo Parts. All to ensure your bus keeps running, ensuring superior uptime, reliability and safety.</p>
                </div>
            </div>
            <!--/ row -->
        </div>

        <!-- ******************************************
        BRANDS
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Personalisation</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row clients">
                    <div class="col-md-4 col-sm-3 col-xs-6">
                        <img src="uploads/volvo/2-5.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-4 col-sm-3 col-xs-6">
                        <img src="uploads/volvo/3-6.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-4 col-sm-3 col-xs-6">
                        <img src="uploads/volvo/b2-1.jpg" alt="" class="img-responsive">
                    </div>                   
                </div><!-- end carousel -->
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>