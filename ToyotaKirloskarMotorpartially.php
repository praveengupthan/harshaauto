<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/grid-gallery.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">       
         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Blog</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="blog.php">Blog</a></li>
                                    <li class="active">Toyota Kirloskar Motor partially resumes Retail & After-Sales Operations</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="row blog-list">
                            <div class="col-md-12">
                                <div class="blog-dark">
                                    <div class="post-media entry">
                                        <img src="uploads/single_car_01.png" alt="" class="img-responsive">
                                        <div class="magnifier colorized">
                                            <a href="#"><i class="flaticon-link"></i></a>
                                        </div>
                                    </div><!-- end media -->

                                    <div class="blog-details wbg">
                                        <div class="alignleft hidden-xs">
                                            <p>25<small>May</small></p>
                                        </div>
                                        <h4>Toyota Kirloskar Motor partially resumes Retail & After-Sales Operations</h4>   
                                        <ul class="list-inline hidden-xs">
                                            <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                            <!-- <li><a href="#"><i class="fa fa-eye"></i> 58 Views</a></li>
                                            <li><a href="#"><i class="fa fa-comment-o"></i> 10 Comments</a></li>
                                            <li><a href="#"><i class="fa fa-tag"></i> Automotive</a></li> -->
                                        </ul>
                                    </div><!-- end meta -->

                                    <div class="blog-meta-desc">
                                        <p>New Delhi, May 12 (PTI) Toyota Kirloskar Motor (TKM) on Tuesday said its dealerships and aftersales operations have partially resumed after weeks of nationwide lockdown leading to complete shutdown of manufacturing and retail units.</p>
                                        
                                        <p>The company said 171 dealership outlets are now functional, while close to 146 service outlets are also operational nationally.</p>
                                
                                        <p>All dealerships will practice stringent social distancing with government prescribed percentage of the workforce at a given point.</p>

                                        <p>The company will also continue to closely review the developments in each region and will take necessary steps basis the eventualities that may arise in the future, TKM said in a statement.</p>
                                        
                                        <p>Commenting on the partial resumption of dealerships and service outlets, TKM Managing Director Masakazu Yoshimura said, 'As we gradually recommence operations, we are ensuring the safety and wellbeing of all our stakeholders while simultaneously safeguarding business continuity.' Keeping in mind the directives prescribed by the government as well as the resumption challenges, he said, 'We have devised certain restart guidelines which will further guide our partners and the industry through the 'new normal', adjusting to the new norms and the changes brought along with this crisis.' TKM said close to 75 per cent of its suppliers have received nod from the government to recommence operations, while the remaining are expected to receive the necessary permission soon.</p>

                                        <p>The company further said it has started preparatory operations at its manufacturing plant from May 5, and operations will resume in a 'phased manner, most importantly keeping in mind the needs of 'social distancing' and sanitization'. PTI RKL RVK </p>
                                    </div><!-- end blog-meta-desc -->

                                    <div class="post-share clearfix">
                                        <div class="pull-left">
                                            <ul class="list-inline">
                                                <li><h4>Did You Like This Post? Please Share on</h4></li>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            </ul><!-- end ul -->
                                        </div>

                                      
                                    </div><!-- end share -->
                                   
                                 
                                </div><!-- end blog-wrapper -->
                            </div><!-- end col --> 
                        </div><!-- end row -->   
                    </div><!-- end col -->

                 
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

    


        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

       

       
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <script src="js/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
    <?php include 'scripts.php' ?>
   

</body>

</html>