<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <?php include 'header.php'?>
        <!-- ******************************************
        START SLIDER 
        ********************************************** -->
        <div class="slider-section">
            <div id="banner">
                <div class="rev_slider_wrapper">
                    <!-- START REVOLUTION SLIDER 5.0 auto mode -->
                    <div id="main_slider" class="rev_slider" data-version="5.0">
                        <ul>
                            <li data-index='rs-377' data-transition='curtain-1' data-slotamount='default'
                                data-easein='default' data-easeout='default' data-masterspeed='default'
                                data-thumb='uploads/slider_01.jpg' data-rotate='0' data-saveperformance='off'
                                data-title='Business Solutions' data-description=''>
                                <!-- MAIN IMAGE -->
                                <img src="uploads/slider_01.jpg" alt="image" data-bgposition="top center"
                                    data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg">
                                <!-- LAYERS -->
                               
                            </li>
                            <li data-index='rs-376' data-transition='curtain-1' data-slotamount='default'
                                data-easein='default' data-easeout='default' data-masterspeed='default'
                                data-thumb='uploads/slider_02.jpg' data-rotate='0' data-saveperformance='off'
                                data-title='Business Solutions' data-description=''>
                                <!-- MAIN IMAGE -->
                                <img src="uploads/slider_02.jpg" alt="image" data-bgposition="top center"
                                    data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg">
                                <!-- LAYERS -->
                               
                            </li>
                            <li data-index='rs-379' data-transition='curtain-1' data-slotamount='default'
                                data-easein='default' data-easeout='default' data-masterspeed='default'
                                data-thumb='uploads/slider_03.jpg' data-rotate='0' data-saveperformance='off'
                                data-title='Business Solutions' data-description=''>
                                <!-- MAIN IMAGE -->
                                <img src="uploads/slider_03.jpg" alt="image" data-bgposition="top center"
                                    data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg">
                                <!-- LAYERS -->
                              
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END REVOLUTION SLIDER -->
            </div> <!-- End of #banner -->
        </div><!-- end slider section -->

        <!-- ******************************************
        FIRST SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">             

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix">
                            <h4>New Vehicles</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_01.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->
                                <!-- <div class="car-price">
                                    <p>₹ 6.98 Lakh</p>
                                </div>                                -->
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">Introducing the new GLANZA GMT</a></h4>
                            </div><!-- end car-title -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_02.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->
                                <!-- <div class="car-price">
                                    <p>₹ 24,42,643</p>
                                </div> -->
                                
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">The Ride That Makes You Feel At Home.</a></h4>
                            </div><!-- end car-title -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_03.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->
                                <!-- <div class="car-price">
                                    <p>₹ 56,95,000</p>
                                </div>                                -->
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">DRIVEN BY INTUITION</a></h4>
                            </div><!-- end car-title -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->
                    <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_04.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->
                                <!-- <div class="car-price">
                                    <p>₹ 56,280 </p>
                                </div>                               -->
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">It's time to up your cool quotient with the all new Maestro edge </a></h4>
                            </div><!-- end car-title -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix">
                            <h4>Events</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_05.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->                                                      
                            </div><!-- end post-media -->

                           
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_06.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->                               
                                
                            </div><!-- end post-media -->

                          
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_07.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier -->
                            </div><!-- end post-media -->

                            
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper clearfix">
                            <div class="post-media entry">
                                <img src="uploads/car_08.jpg" alt="" class="img-responsive">
                                <div class="magnifier">
                                </div><!-- end magnifier --> 
                            </div><!-- end post-media -->
                        </div><!-- end clearfix -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PARALLAX
        ********************************************** -->
        <div class="parallax section" data-stellar-background-ratio="0.5"
            style="background-image:url('uploads/parallax_01.jpg');">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-car"></i>
                            <p class="stat_count">2545</p>
                            <small>New Cars For Sale</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-profile"></i>
                            <p class="stat_count">2545</p>
                            <small>Satisfied Customers</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-vehicle"></i>
                            <p class="stat_count">2545</p>
                            <small>Dealer Branches</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-signs"></i>
                            <p class="stat_count">1008</p>
                            <small>Certifications Hold</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        BANNERS & DEALS
        ********************************************** -->
        <div class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Special Offers</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper deal-wrapper clearfix">
                            <div class="post-media">
                                <img src="uploads/deal_01.jpg" alt="" class="img-responsive">
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">MAKE EVERY DRIVE AN ADVENTURE.</a></h4>
                            </div><!-- end car-title -->
                          
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper deal-wrapper clearfix">
                            <div class="post-media">
                                <img src="uploads/deal_02.jpg" alt="" class="img-responsive">
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">Renault TRIBER | Space for everything</a></h4>
                            </div><!-- end car-title -->
                           
                        </div><!-- end clearfix -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="car-wrapper deal-wrapper clearfix">
                            <div class="post-media">
                                <img src="uploads/deal_03.jpg" alt="" class="img-responsive">
                            </div><!-- end post-media -->

                            <div class="car-title clearfix">
                                <h4><a href="#">The legend gets bolder</a></h4>
                            </div><!-- end car-title -->                            
                        </div><!-- end clearfix -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis">

                <div class="row">
                    <div class="col-md-6 col-sm-6 wow fadeInUp">
                        <div class="post-media">
                            <a href="#"><img src="uploads/banner_01.jpg" alt="" class="img-responsive"></a>
                        </div><!-- end post-media -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-6 wow fadeInUp">
                        <div class="post-media">
                            <a href="#"><img src="uploads/banner_02.jpg" alt="" class="img-responsive"></a>
                        </div><!-- end post-media -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        MESSAGE BOXES
        ********************************************** -->
        <div class="section nopadding">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-sm-6 color1">
                        <div class="message-box row clearfix">
                            
                            <div class="col-md-12">
                                <h3>Looking For Buy a Car?</h3>
                                <p>Now is a good time to buy a car, Engines provide you new and used car in good
                                    conditions, after full car checkup only we deleiverd your car to you with fully
                                    completed documentations. </p>

                                <a href="#" class="btn btn-default">BUY A CAR</a>
                            </div><!-- end col -->
                        </div><!-- end messahe box -->
                    </div><!-- emd col -->

                    <div class="col-md-4 col-sm-6 color2">
                        <div class="message-box row clearfix">                            
                            <div class="col-md-12">
                                <h3>Do You Want to Sell a Car?</h3>
                                <p>Now is a good time to buy a car, Engines provide you new and used car in good
                                    conditions, after full car checkup only we deleiverd your car to you with fully
                                    completed documentations. </p>

                                <a href="#" class="btn btn-default">SELL A CAR</a>
                            </div><!-- end col -->
                        </div><!-- end messahe box -->
                    </div><!-- emd col -->

                    <div class="col-md-4 col-sm-12 color1">
                        <div class="message-box row clearfix">
                           
                            <div class="col-md-12">
                                <h3>Toyota Kirloskar Motor partially resumes Retail & After-Sales Operations</h3>
                                <p>After weeks of nationwide lockdown leading to complete shutdown of manufacturing and retail units, Toyota Kirloskar Motor announced the partial resumption of Dealer and After Sales operations. </p>

                                <a href="#" class="btn btn-default">KNOW MORE</a>
                            </div><!-- end col -->
                        </div><!-- end messahe box -->
                    </div><!-- emd col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        WHY CHOOSE US
        ********************************************** -->
        <div class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Why Choose Us</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-box clearfix">
                            <!-- <i class="flaticon-people alignleft"></i> -->
                            <h4>Auto Loan Facility</h4>
                            <small>Own your dream ride with car loans.</small>
                            <p>Get a car loan to purchase a vehicle of your choice. With our loans, you can buy a hatchback, sedan, MUV, SUV, sports car or a luxury car. Our auto loans are available to salaried employees, businessmen, professionals, corporates as well as NRIs and PIOs. We offer attractive interest rates on car loans with up to 90% financing, making our loans the ideal choice for funding your brand-new car.</p>
                            <p></p>
                        </div><!-- end service-box -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-box clearfix">
                            <!-- <i class="flaticon-tool alignleft"></i> -->
                            <h4>Free Documentation:</h4>
                            <small>No Hidden Charges</small>
                            <p>The car loan documents required are pretty much the same for most financial service providers. The documentation process is usually simpler for salaried professionals. For self-employed individuals, some additional documents such as income tax reports are required to be submitted.</p>
                        </div><!-- end service-box -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="service-box clearfix">                           
                            <h4>Customer Support</h4>
                            <small>24/7 Online Support</small>
                            <p>We’re here to help. Whether you have questions about your vehicle, our products, navigation, Warranty and more, your answers are just a click away. It’s available 24/7. Please visit our Frequently Asked Questions today.</p>
                        </div><!-- end service-box -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PARALLAX LAST
        ********************************************** -->
        <div class="parallax section" data-stellar-background-ratio="0.5"
            style="background-image:url('uploads/parallax_02.jpg');">
            <div class="container bgcolor">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="about-widget">
                            <div class="section-title clearfix">
                                <h4>Thousands of Customers Behind Our Success. </h4>
                                <hr class="custom">
                            </div><!-- end section-title -->

                            <p>We’ve had some really happy customers in the past, and we wish we find one in you too. In the meanwhile, here is what they had to say about our services. You can sort through the testimonial’s basis the car model from the drop-down list.</p>

                            <a href="#" class="readmore">Know More</a>

                        </div><!-- end about-widget -->
                    </div><!-- end col -->

                    <div class="col-md-8 col-sm-12">
                        <div class="testi-carousel owl-carousel owl-theme">
                            <div class="testimonial clearfix">
                                <p class="lead">Toyota is always the best brand. And the service here is good.</p>
                                <div class="testi-meta">
                                  
                                    <h4>Sandy Medapati </h4>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- end rating -->
                                </div><!-- end testi-meta -->
                            </div><!-- end testimonial -->

                            <div class="testimonial clearfix">
                                <p class="lead">They have got my project on time with the competition with a highly
                                    skilled, well-organized and experienced team of professional team mates how all this
                                    mistaken idea of and praising a complete account of the system.</p>
                                <div class="testi-meta">
                                   
                                    <h4>James Fernando <small>- Manager of Racer</small></h4>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- end rating -->
                                </div><!-- end testi-meta -->
                            </div><!-- end testimonial -->
                        </div><!-- end carousel -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>