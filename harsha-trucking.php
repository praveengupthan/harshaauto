<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">

        <!-- banner-->
        <div class="banner-groupitem">
            <img src="uploads/truck_new.jpg" alt="" class="img-responsive">
        </div>
        <!--/ banner -->

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Harsha Trucking</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="javascript:void(0)">Group Companies</a></li>
                                    <li class="active">Harsha Trucking</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="post-media service-img">
                            <img src="uploads/trucking01.png" alt="" class="img-responsive">
                        </div><!-- end post-media -->                                  
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12 m30">
                        <div class="section-title small-margin-title clearfix">
                            <h5>Welcome To Harsha Trucking</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->            
                        <div class="service-text">                        
                            <p>Harsha Automotive expanded its business in 2013 by partnering with German based automotive company “BharatBenz”. It takes care of sales & services of Bharat Benz trucks with established dealer points at Hyderabad, Kodad & Ananthapur. Thus covering Telangana & part of Rayalaseema, AP states. “Harsha Group is a leading auto-mobile conglomerate in Retail Chain Outlets in Southern India under the name Sri Harsha Trucking representing DAIMLER for the Brand Bharatbenz Trucks and Buses in state of Telangana stretching up to Kurnool and Anantapur in state of Andhrapradesh. Milestones Achieved : Increased market share in 2015 and have been awarded by DAIMLER. Logged and crossed the 1000 Bharatbenz Trucks in 2015, has been accredited with GOLD and PLATINUM ratings by DAIMLER for INFRASTRUCTURE creation and management process” Placing customer satisfaction first and integrating sales with service and service parts in a single convenient location, we contribute to speedy and efficient service, allowing customers to experience the convenience and pleasure of owning BharatBenz Trucks.</p> 
                        </div><!-- end service-text -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->
       

      



        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section db">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-left">
                            <h4>Harsha Hero Branches</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">                   
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Hyderabad</h5>                            
                            <p>Sri Harsha Trucking, Sy.No.286/287/288/289/P, Bagh Hayath, Hayathnagar(Mandal), Rangareddy (Dist),
                                Telangana - 501505.</p>
                            <p>Phone no: 040-24113115/6/7/8</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        CALLBOX
        ********************************************** -->
        <div class="section bg callmewrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <h2><i class="flaticon-customer-service"></i> Please Call our Toll free number!</h2>
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <h2><i class="flaticon-phone-call"></i> +91 40-24113115</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        BRANDS
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Genuine Parts</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <!-- row -->
                <div class="row">

                <!-- col -->
                <div class="col-lg-4 text-center">
                    <img src="uploads/gtrucking01.png" class="img-responsive">
                    <h4 class="h2">Reliable Quality</h4>
                    <p>Every BharatBenz genuine part undergoes stringent quality checks to ensure it meets the BharatBenz global quality standards before it reaches to our customers.</p>
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <img src="uploads/gtrucking02.png" class="img-responsive">
                    <h4 class="h2">Available Closer To You</h4>
                    <p>BharatBenz Genuine Parts are available to all our customers across India at your convenience.</p>
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <img src="uploads/gtrucking03.png" class="img-responsive">
                    <h4 class="h2">Affordability</h4>
                    <p>Usage of BharatBenz Genuine Parts ensure longer aggregate life which translates to minimum maintenance and repair cost, improved vehicle performance and maximum profitability.</p>
                </div>
                <!--/ col -->

                </div>
                <!--/ row -->

              
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>