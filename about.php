<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>About Us</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">About Us</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="welcome-widget clearfix">
                            <div class="section-title clearfix">
                                <h4>Who we are!</h4>
                                <hr class="custom">
                            </div><!-- end section-title -->

                            <p>Sri. Muppavarapu Harshavardhan is a young dynamic Entrepreneur graduated in Commerce and hails from a respectable family with a public image. He is the native of Nellore, Andhra Pradesh and presently residing at Hyderabad. He is well known business leader in Automobile and hospitality industries. His unshakable belief that India will never achieve its true growth story until the rural sector of the country is empowered to make choices and transform their own lives. With this aspiration he started the foundation along with his family members Smt. M.Radha and Smt. M Usha.</p>

                            <p>A name of trust for many leading automobile brands like Toyota, Hero MotoCorp, Bharat Benz, Volvo etc. as a reliable channel partner, the Harsha Automotive Group started its foray into a highly customer centric division of Automobile industry as designated channel partner of Hero Honda for sales, service & spare parts, Vizag in the year 2000.</p>
                          
                        </div><!-- end welcome-widget -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 welcome-widget wow fadeIn">
                        <img src="uploads/about.png" alt="" class="img-responsive">
                    </div>

                    <div class="col-sm-12">
                        <p>Excellence in business operations and complete focus on Manpower Development made Harsha group to emerge into a multi manufacturer Channel Partner across different segments of the industry with association being expanded to Bharat Benz, Volvo India Pvt Ltd etc…</p>

                        <p>The former one located in the posh residential locality of the city i.e. at Jubilee Hills and the later restaurant located in the heart of the IT hub Hi-Tech city. Earned a reputation as one of its kind eatery hang-outs among the people in Hyderabad for the consistency in taste, service & quality. </p>

                        <p>With Quality and Customer Satisfaction becoming the guidelines for our business operations and complete focus on Manpower Development as the direction, Harsha Group grew exponentially in the competitive field of Automobile Industry to achieve a high level of success in a very short span of 14 years.</p>
                    </div>
                </div><!-- end row -->

                <hr class="large">
              
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PARALLAX
        ********************************************** -->
        <div class="parallax section" data-stellar-background-ratio="0.7" style="background-image:url('uploads/parallax_04.jpg');">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-car"></i>
                            <p class="stat_count">2545</p>
                            <small>New Cars For Sale</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-profile"></i>
                            <p class="stat_count">2545</p>
                            <small>Satisfied Customers</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-vehicle"></i>
                            <p class="stat_count">2545</p>
                            <small>Dealer Branches</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-3 col-xs-12 wow fadeIn">
                        <div class="stat-wrap">
                            <i class="flaticon-signs"></i>
                            <p class="stat_count">1008</p>
                            <small>Certifications Hold</small>
                        </div><!-- end stat-wrap -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        SERVICES SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Our Advantages</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row about-list">
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-home"></i>
                            <h4>25 years of Experience</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-profile"></i>
                            <h4>Exclusive Partnership</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-technology-2"></i>
                            <h4>Innovative Workers</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis">

                <div class="row about-list">
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-tool-1"></i>
                            <h4>Best Quality Products</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-hands"></i>
                            <h4>Business Opportunities</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-clock"></i>
                            <h4>24/7 Online Support</h4>
                            <!-- <p class="showhover">Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p> -->
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->
       
        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Our Groups</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row clients">
                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_01.jpg" alt="" class="img-responsive">
                    </div>
                   
                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_03.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <img src="uploads/client_04.jpg" alt="" class="img-responsive">
                    </div>                  
                  
                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_09.jpg" alt="" class="img-responsive">
                    </div>

                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_02.jpg" alt="" class="img-responsive">
                    </div>

                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_05.jpg" alt="" class="img-responsive">
                    </div>

                    <div class=" col-sm-3 col-xs-6">
                        <img src="uploads/client_06.jpg" alt="" class="img-responsive">
                    </div>
                </div><!-- end carousel -->
            </div><!-- end container -->
        </div><!-- end section -->

        </section>
        <!--/ sub apge ends -->

        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>