<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Latest Vehicles</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">Latest Vehicles</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->
        <div class="section">
            <div class="container">               
                <!-- row -->
                <div class="row">

                <!-- col -->
                <div class="col-md-4">
                    <div class="vehicle-col">
                        <a href="ToyotaGlanzaDetail.php"><img src="uploads/toyota-glanza.png" alt="" class="img-responsive"></a>
                        <article>
                            <h3 class="h4">Toyota Glanza</h3>
                            <h6 class="h6">Rs:6.97 - 8.9 Lakh (Approximately)</h6>
                            <!-- <p class="small list-ve">
                                <span>23.87 kmpl</span>
                                <span>1197 cc</span>
                                <span>Petrol</span>
                            </p> -->
                            <a class="link-car" href="ToyotaGlanzaDetail.php">Know More</a>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-md-4">
                    <div class="vehicle-col">
                        <a href="vehicle-detail.php"><img src="uploads/toyota-Vellfire.png" alt="" class="img-responsive"></a>
                        <article>
                            <h3 class="h4">Toyota Vellfire</h3>
                            <h6 class="h6">Rs:79.5 Lakh to 82.5 Lakh(Approximately) </h6>
                            <!-- <p class="small list-ve">                               
                                <span>2494 cc</span>
                                <span>Petrol</span>
                            </p> -->
                            <a class="link-car">Know More</a>
                           
                        </article>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-md-4">
                    <div class="vehicle-col">
                        <a href="vehicle-detail.php"><img src="uploads/carmy.png" alt="" class="img-responsive"></a>
                        <article>
                            <h3 class="h4">Toyota Camry</h3>
                            <h6 class="h6">Rs:37.88 Lakh to 40.22 Lakh(Approximately)</h6>
                            <!-- <p class="small list-ve">
                                <span>19.16 kmpl</span>
                                <span>2487 cc</span>
                                <span>Petrol</span>
                            </p> -->
                            <a class="link-car">Know More</a>
                           
                        </article>
                    </div>
                </div>
                <!--/ col -->
                   
                </div>
                <!--/ row -->
                
            </div><!-- end container -->
        </div><!-- end section -->

       


        

        
        </section>
        <!--/ sub apge ends -->
        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>