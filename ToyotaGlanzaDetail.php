<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Toyota Glanza</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="newvehicles.php">Latest Vehicles</a></li>
                                    <li class="active">Toyota Glanza</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="single-car-wrapper clearfix">
                            <!-- main slider carousel -->
                            <div class="row">
                                <div class="col-md-12" id="slider">
                                    <div class="col-md-12" id="carousel-bounding-box">
                                        <div id="myCarousel" class="carousel slide">
                                            <!-- main slider carousel items -->
                                            <div class="carousel-inner">
                                                <div class="active item" data-slide-number="0">
                                                    <img src="uploads/glanza/glanza01.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="item" data-slide-number="1">
                                                    <img src="uploads/glanza/glanza02.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="item" data-slide-number="2">
                                                    <img src="uploads/glanza/glanza03.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="item" data-slide-number="3">
                                                    <img src="uploads/glanza/glanza04.png" alt="" class="img-responsive">
                                                </div>
                                                <div class="item" data-slide-number="4">
                                                    <img src="uploads/glanza/glanza05.png" alt="" class="img-responsive">
                                                </div>
                                            </div>
                                            <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
                                            <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/main slider carousel-->

                            <!-- thumb navigation carousel -->
                            <div class="row-fluid" id="slider-thumbs">
                                <!-- thumb navigation carousel items -->
                                <ul class="list-inline">
                                    <li class="col-md-15 col-sm-15 col-xs-6">
                                        <a id="carousel-selector-0" class="selected">
                                            <img src="uploads/glanza/glanza01.png" alt="" class="img-responsive">
                                        </a>
                                    </li>
                                    <li class="col-md-15 col-sm-15 col-xs-6">
                                        <a id="carousel-selector-1">
                                            <img src="uploads/glanza/glanza02.png" alt="" class="img-responsive">
                                        </a>
                                    </li>
                                    <li class="col-md-15 col-sm-15 col-xs-6">
                                        <a id="carousel-selector-2">
                                            <img src="uploads/glanza/glanza03.png" alt="" class="img-responsive">
                                        </a>
                                    </li>
                                    <li class="col-md-15 col-sm-15 col-xs-6">
                                        <a id="carousel-selector-3">
                                            <img src="uploads/glanza/glanza04.png" alt="" class="img-responsive">
                                        </a>
                                    </li>
                                    <li class="col-md-15 col-sm-15 col-xs-6">
                                        <a id="carousel-selector-4">
                                            <img src="uploads/glanza/glanza05.png" alt="" class="img-responsive">
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <div class="car-description clearfix">
                                <h3>Toyota Glanza</h3>

                                <p>Toyota Glanza Launch and Variants: The Glanza is available in two variants: G and V,  which are priced from Rs 7.22 lakh to Rs 8.90 lakh (ex-showroom Delhi). </p>

                                <p>Toyota Glanza Engines: The Glanza is a petrol-only offering. It gets both the new 1.2-litre motors that are now offered on the Baleno. The new Dual Jet mild-hybrid engine makes 90PS of power and 113Nm of torque with a 5-speed MT.  Meanwhile, the regular 1.2-litre engine makes 83PS and 113Nm, and is available with a 5-speed MT as well as a CVT. Here are the fuel economy figures of the Toyota Glanza:</p>

                                <ul>
                                    <li>1.2-litre petrol MT- 21.01kmpl</li>
                                    <li>1.2-litre petrol mild hybrid MT- 23.87kmpl</li>
                                    <li>1.2-litre petrol CVT- 19.56kmpl</li>
                                </ul>

                                <p>Toyota Glanza Features: Just like its styling, the Glanza also shares its feature list with the Baleno. Dual front airbags, ABS with EBD, ISOFIX child seat anchors and rear parking sensors are available as standard. Other features on offer include auto LED projector headlamps, LED DRLs, fog lamps, alloy wheels, a 7-inch touchscreen infotainment system with Apple CarPlay and Android Auto, auto-dimming IRVM and auto AC.</p>
                                <p>Toyota Glanza Rivals: The Glanza competes with the Maruti Baleno, Hyundai Elite i20, Volkswagen Polo, Honda Jazz and the recently launched Tata Altroz in India.</p>
                            </div><!-- end desc -->

                            <div class="car-table clearfix">
                                <p><strong>Key Specifications</strong> of Toyota Glanza </p>
                                <i class="fa fa-angle-down"></i>
                            </div><!-- end car-table -->    

                            <div class="table-responsive">
                                <table class="table car-table-wrapper">
                                    <tbody>
                                        <tr>
                                            <td>ARAI Mileage</td>
                                            <td><strong>19.56 kmpl</strong></td>
                                            <td>Fuel Type</td>
                                            <td><strong>Petrol </strong></td>
                                        </tr>
                                        <tr>
                                            <td>Engine Displacement (cc)</td>
                                            <td><strong>1197</strong></td>
                                            <td>Max Power (bhp@rpm)</td>
                                            <td><strong>82bhp@6000rpm</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Max Torque (nm@rpm)</td>
                                            <td><strong>113Nm@4200rpm</strong></td>
                                            <td>Seating Capacity</td>
                                            <td><strong>5</strong></td>
                                        </tr>
                                        <tr>
                                            <td>TransmissionType</td>
                                            <td><strong>Automatic</strong></td>
                                            <td>Boot Space (Litres)</td>
                                            <td><strong>339</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Fuel Tank Capacity</td>
                                            <td><strong>37</strong></td>
                                            <td>Body Type</td>
                                            <td><strong>Hatchback</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Service Cost (Avg. of 5 years)</td>
                                            <td><strong>Rs.3,417</strong></td>
                                            <td>Power Steering</td>
                                            <td><strong>Yes</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- end table-responsive -->

                            <div class="car-table clearfix">
                                <p><strong>Technical Details</strong> of Toyota Glanza </p>
                                <i class="fa fa-angle-down"></i>
                            </div><!-- end car-table -->    

                            <!-- tabs left -->
                            <div class="tabbable tabs-left row-fluid clearfix">
                                <ul class="nav nav-tabs col-md-2 col-sm-2">
                                    <li><a href="#a" data-toggle="tab">Engine and Transmission </a></li>
                                    <li class="active"><a href="#b" data-toggle="tab">Fuel & Performance</a></li>
                                    <li><a href="#c" data-toggle="tab">Suspension, Steering & Brakes</a></li>
                                    <li><a href="#d" data-toggle="tab">Dimensions & Capacity </a></li>
                                </ul>
                                <div class="tab-content col-md-10 col-sm-10">
                                    <div class="tab-pane fade clearfix" id="a">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Engine Type
                                                        <strong>Petrol Engine</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Displacement (cc)
                                                        <strong>1197</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Max Power (bhp@rpm)
                                                        <strong>82bhp@6000rpm</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Max Torque (nm@rpm)
                                                        <strong>113Nm@4200rpm</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        No. of cylinder
                                                        <strong>4</strong>
                                                    </li>
                                                </ul>
                                            </div><!-- end col -->
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Valves Per Cylinder
                                                        <strong>4</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Transmission Type
                                                        <strong>Automatic</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Gear Box
                                                        <strong>5-Speed</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Drive Type:
                                                        <strong>FWD</strong>
                                                    </li>                                                    
                                                </ul>
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end tab-pane -->
                                    <div class="tab-pane fade in active" id="b">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Fuel Type
                                                        <strong>Petrol</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Mileage (ARAI)
                                                        <strong>19.56</strong>
                                                    </li>
                                                   
                                                   
                                                </ul>
                                            </div><!-- end col -->
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Fuel Tank Capacity (Litres)
                                                        <strong>37</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Emission Norm Compliance
                                                        <strong>BS VI</strong>
                                                    </li>
                                                   
                                                </ul>
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end tab-pane -->
                                    <div class="tab-pane fade" id="c">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Front Suspension
                                                        <strong>MacPherson Strut</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Rear Suspension
                                                        <strong>Torsion Beam</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Steering Type
                                                        <strong>Power</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Steering Column
                                                        <strong>Tilt & Telescopic</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Turning Radius (Metres)
                                                        <strong>4.9m</strong>
                                                    </li>
                                                </ul>
                                            </div><!-- end col -->
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Front Brake Type
                                                        <strong>Disc</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Rear Brake Type
                                                        <strong>Drum</strong>
                                                    </li>
                                                    
                                                </ul>
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end tab-pane -->
                                    <div class="tab-pane fade" id="d">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Length (mm)
                                                        <strong>3995</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Width (mm)
                                                        <strong>1745</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Height (mm)
                                                        <strong>1510</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Boot Space (Litres)
                                                        <strong>339</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Seating Capacity
                                                        <strong>5</strong>
                                                    </li>
                                                </ul>
                                            </div><!-- end col -->
                                            <div class="col-md-6 col-sm-12">
                                                <ul class="car-list-wrapper-2">
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Wheel Base (mm)
                                                        <strong>2520</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Kerb Weight (Kg)
                                                        <strong>935</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        Gross Weight (Kg)
                                                        <strong>1360</strong>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-angle-double-right alignleft"></i>
                                                        No of Doors
                                                        <strong>5</strong>
                                                    </li>                                                    
                                                </ul>
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end tab-pane -->
                                </div>
                            </div>
                            <!-- /tabs -->                           
                        </div><!-- end single-car-wrapper -->
                    </div><!-- end col -->
                  
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->
       

       


        

        
        </section>
        <!--/ sub apge ends -->
        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

    <script type="text/javascript">
        (function($) {
        "use strict";
        $('#myCarousel').carousel({
            interval: 4000
            });

            // handles the carousel thumbnails
            $('[id^=carousel-selector-]').click( function(){
              var id_selector = $(this).attr("id");
              var id = id_selector.substr(id_selector.length -1);
              id = parseInt(id);
              $('#myCarousel').carousel(id);
              $('[id^=carousel-selector-]').removeClass('selected');
              $(this).addClass('selected');
            });

            // when the carousel slides, auto update
            $('#myCarousel').on('slid', function (e) {
              var id = $('.item.active').data('slide-number');
              id = parseInt(id);
              $('[id^=carousel-selector-]').removeClass('selected');
              $('[id=carousel-selector-'+id+']').addClass('selected');
        });
        })(jQuery);
    </script>

</body>

</html>