<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">

        

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Events</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>                                    
                                    <li class="active">Events</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row blog-list">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/event01.jpg" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="#">Anti Drug</a></h4>   
                                <p>Today on 30th July 2017, Harsha Toyota team of Harsha Auto Group supported and enthusiastically participated in Anti - Drug Walk at 7 AM which was held at KBR Park, Hyderabad. In this event NDA's nominated Vice Presidential candidate Hon'ble Sri  M Venkaiah Naidu Garu also actively participated along with other regional politicians and conveyed the message to the youth not to get addicted to drugs. The Anti Drug Walk campaign was organized to raise the voice and fight against the drugs menace.</p>
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Jack</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 58 Views</a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i> 10 Comments</a></li>
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/event02.jpg" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="#">Awareness Toyota- Harsha Toyota</a></h4>   
                                <p>To commemorate the World Environment Day, Harsha Toyota, Kurnool in association with SIAM announces “Free Pollution Check” program on June 5th 2018. Aimed at spreading awareness on environment, this initiative by Harsha Toyota substantiates company’s commitment towards the society and the environment.   Toyota customers can get a “Free Pollution Check”.The “Free Pollution Check” will incorporate emission checking equipment and devices to clean the engine. The customers availing of the unique initiative will be assisted by technical personnel at the Harsha Toyota.</p>
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Mano</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 93 Views</a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i> 10 Comments</a></li>
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 
                   
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/event04.jpg" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="#">Harsha Toyota Ananthapur Imparting Institutional Training:
</a></h4>   
                                <p>Harsha Auto Enterprises Pvt Ltd(Harsha Toyota) is very glad to inform that
it has received an award from Anantapur Superindent of police for imparting Institutional Training in a highly professional Manner to 151 SCT PCs(Mechanics and Drivers) of Police Transport Organization, Andhra Pradesh. This training was held from 17/4/2018 to 30/4/2018
at Harsha Auto Enterprises Pvt ltd, Anantapuram Branch.</p>
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Mano</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 12 Views</a></li>
                                    <li><a href="#"><i class="fa fa-comment-o"></i> 2 Comments</a></li>
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 
                  
                </div><!-- end row -->

                <hr class="invis2">                            
              
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>