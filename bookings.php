<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Booking</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">Bookings</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->
        <div class="section" style="padding-top:0;">
            <!-- image -->
            <div class="booking-banner">
                <img src="uploads/slider_01.jpg" class="img-responsive" style="width:100%">
            </div>
            <!--/ image -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="">
                            <div class="row">                                

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 submit_form shipping_address">

                                <div class="section-title clearfix">
                                        <h5>DEALERSHIP DETAILS</h5>
                                        <hr class="custom">
                                    </div><!-- end section-title -->
                                    <form action="#" class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                          
                                            <span>Select City *</span>
                                            <div class="form-group">
                                                <select class="form-control select-drop w-100">
                                                    <option>Hyderabad</option>
                                                    <option>Secunderabad</option>
                                                    <option>Nizamabad</option>
                                                    <option>Secunderabad</option>
                                                </select>
                                            </div>                                           
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                          
                                            <span>Select Nearest Dealer *</span>
                                            <div class="form-group">
                                                <select class="form-control select-drop w-100">
                                                    <option>Radha Krishna Toyota, Sanath Nagar</option>
                                                    <option>Radha Krishna Toyota, Uppal</option>
                                                    <option>Radha Krishna Toyota, Hyderaguda</option>                                                   
                                                </select>
                                            </div>                                           
                                        </div>                                       
                                    </form>


                                    <div class="section-title clearfix">
                                        <h5>CAR MODEL DETAILS:</h5>
                                        <hr class="custom">
                                    </div><!-- end section-title -->
                                    <form action="#" class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                          
                                            <span>Car Model *</span>
                                            <div class="form-group">
                                                <select class="form-control select-drop w-100">
                                                    <option>Glanza</option>
                                                    <option>Yaris</option>
                                                    <option>Fortuner</option>
                                                    <option>Innova Crysta</option>
                                                </select>
                                            </div>                                           
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <span>Old Car Exchange *</span>                                           
                                               <label class="float-left" style="margin-right:10px;">
                                                    <input type="radio"> Yes
                                               </label>      
                                               <label>
                                                    <input type="radio"> No
                                               </label>                        
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <span>Finance Required *</span>                                           
                                               <label class="float-left" style="margin-right:10px;">
                                                    <input type="radio"> Yes
                                               </label>      
                                               <label>
                                                    <input type="radio"> No
                                               </label>                        
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                          
                                            <span>Initial booking Amount *</span>
                                            <div class="form-group">
                                                <select class="form-control select-drop w-100">
                                                    <option>10,0000</option>
                                                    <option>20,000</option>
                                                    <option>30,000</option>                                                   
                                                </select>
                                            </div>                                           
                                        </div>    
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                          
                                            <span>Initial booking Amount *</span>
                                            <div class="form-group">
                                                <select class="form-control select-drop w-100">
                                                    <option>10,0000</option>
                                                    <option>20,000</option>
                                                    <option>30,000</option>                                                   
                                                </select>
                                            </div>                                           
                                        </div> 
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <span>Expecting Delivery Date*</span>
                                                <input type="date" class="form-control select-drop" placeholder="Select Date">
                                            </div>                                           
                                        </div><!-- end col -->
                                    </form>
                                </div> <!-- /submit_form -->

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 submit_form shipping_address">
                                    <div class="section-title clearfix">
                                        <h5>PERSONAL INFORMATION::</h5>
                                        <hr class="custom">
                                    </div><!-- end section-title -->
                                    <form action="#" class="row">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Write Name">
                                            </div>                                           
                                        </div>
                                       
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Full Address">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Mobile">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Email">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Aadhar Number">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="name" class="form-control select-drop" placeholder="Pan Number">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Upload Aadhar  *</span>
                                            <div class="form-group">
                                                <input type="file" class="form-control select-drop">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Upload Pan  *</span>
                                            <div class="form-group">
                                                <input type="file" class="form-control select-drop">
                                            </div>                                           
                                        </div>
                                        <p><i style="color:red;">*Files to be uploaded in .pdf or .jpeg and file size should be less than 1MB</i></p>
                                    </form>
                                </div> <!-- /submit_form -->                                
                            </div> <!-- /row -->
                        </div> <!-- /check_out_form -->

                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="h3">Terms and Conditions</h5>
                                <p>
                                    <input type="checkbox"> I agreed to book above mentioned vehicle with Radha Group Toyota and will pay the booking amount as per instructions by sales team if the selected model is available. In case of non availability, the booking amount will be paid upon confirmation. I Understand that this advance booking will ensure priority delivery of my preferred model.
                                </p>
                                <p>
                                    <input type="checkbox"> I agree that Radha Group Sales team will contact me via Phone/WhatsApp/Email for further discussion.
                                </p>
                            </div>
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->
        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>