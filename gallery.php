<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">       
         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Gallery</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li> 
                                    <li><a href="javawscript:void(0)">Media</a></li>                                            
                                    <li class="active">Gallery</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row blog-list">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/blog_01.png" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="gallery-detail.php">Awareness Toyota- Harsha Toyota</a></h4>  
                               
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 15 Pictures</a></li>                                    
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/blog_02.png" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="gallery-detail.php">Catch us live -Corporate Event at IBM Hyd</a></h4>  
                               
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 15 Pictures</a></li>                                    
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/blog_03.png" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="#"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->

                            <div class="blog-details">
                                <h4><a href="gallery-detail.php">Harsha Toyota Yaris Corporate Launch at Infosys Hyderabad</a></h4>
                            </div><!-- end details -->

                            <div class="blog-meta">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                    <li><a href="#"><i class="fa fa-eye"></i> 56 Pictures</a></li>                                    
                                </ul>
                            </div><!-- end meta -->
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col -->                     
                </div><!-- end row --> 
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>
    

</body>

</html>