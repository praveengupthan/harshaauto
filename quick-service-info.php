<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Quick Services</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">Quick Services</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->
        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="post-media service-img">
                            <img src="uploads/service_01.jpg" alt="" class="img-responsive">
                        </div><!-- end post-media -->
                        <div class="section-title small-margin-title clearfix">
                            <h5>General Services</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->

                        <div class="service-text">
                            <p>All this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth praising pain was born account of the system. </p>

                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="customlist">
                                        <li><i class="fa fa-wrench"></i> Replace the engine oil</li>
                                        <li><i class="fa fa-wrench"></i> Check/Replace the fuel filter </li>
                                        <li><i class="fa fa-wrench"></i> Check/Replace windshield wipers</li>
                                        <li><i class="fa fa-wrench"></i> Replace air filter</li>
                                    </ul><!-- end customlist -->
                                </div><!-- end col -->

                                <div class="col-md-6">
                                    <ul class="customlist">
                                        <li><i class="fa fa-wrench"></i> Flush transmission fluid</li>
                                        <li><i class="fa fa-wrench"></i> Clean or replace battery</li>
                                        <li><i class="fa fa-wrench"></i> Inspect or replace brake pads</li>
                                        <li><i class="fa fa-wrench"></i> Inspect or replace timing belt</li>
                                    </ul><!-- end customlist -->
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end service-text -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12 m30">
                        <div class="section-title clearfix">
                            <h4>Our Better Services</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->

                        <ul class="customlist withborder clearfix">
                            <li>
                                <div class="service-box clearfix">
                                    <img src="uploads/service_01.png" alt="" class="img-thumbnail img-circle alignleft wow fadeInLeft">
                                    <h4>Interim Car Service</h4>
                                    <p>The Interim service is recommended every 6 months high-mileage drivers, or where more frequent checks are recommended by your vehicle manufacturer, is very important one... <a href="#">Read More</a></p>
                                </div><!-- end service-box -->
                            </li><!-- end col -->

                            <li>
                                <div class="service-box clearfix">
                                    <img src="uploads/service_02.png" alt="" class="img-thumbnail img-circle alignleft wow fadeInLeft">
                                    <h4>Full Car Service</h4>
                                    <p>Our Full Service exceeds most manufacturers service schedules. If you book a full car service every year, you give yourself the best chance of totally trouble-free motoring... <a href="#">Read More</a></p>
                                </div><!-- end service-box -->
                            </li><!-- end col -->

                            <li>
                                <div class="service-box clearfix">
                                    <img src="uploads/service_03.png" alt="" class="img-thumbnail img-circle alignleft wow fadeInLeft">
                                    <h4>Major Car Service</h4>
                                    <p>This extremely comprehensive service should be undertaken every two years, or snt to make sure all components are working safely and effectively, It covers all areas... <a href="#">Read More</a></p>
                                </div><!-- end service-box -->
                            </li><!-- end col -->
                        </ul><!-- end row -->

                        <a href="#" class="btn btn-default">SCHEDULE NOW</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PARALLAX
        ********************************************** -->

        <div class="parallax section" data-stellar-background-ratio="0.5" style="background-image:url('uploads/parallax_03.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="callout clearfix">
                            <h2>Free service for premium members</h2>
                            <p>Expound the actual teachings of the great explorer of the<br> truth, the master-builder of human happiness. </p>
                            <a href="#" class="btn custombtn">CONTACT US</a>
                        </div><!-- end callout -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end parallax -->

        <!-- ******************************************
        SERVICES SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Full Range Of Services</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-truck-wheel"></i>
                            <h4>Wheel Works</h4>
                            <p>Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-painting-paint-roller"></i>
                            <h4>Painting Works</h4>
                            <p>Praising pain was bon and give you a complete account of the uts system expound the actual teachings.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-technology-1"></i>
                            <h4>Air Conditioner</h4>
                            <p>There are many variationsavailable  but the majority have suffereed seds alteration in some form..</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-hose"></i>
                            <h4>Air Conditioner</h4>
                            <p> Which of usundertakes laborious physical exercise, except to ut obtain some advantage from it.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <hr class="invis">

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-malfunction-indicador"></i>
                            <h4>Engine Works</h4>
                            <p>Know how to pursue pleasure seds encounter consequences that are ut extremely painfull nor pursues.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-oil"></i>
                            <h4>Lube Oil & Filters</h4>
                            <p>Praising pain was bon and give you a complete account of the uts system expound the actual teachings.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-disc-brake"></i>
                            <h4>Brake Repairs</h4>
                            <p>There are many variationsavailable  but the majority have suffereed seds alteration in some form..</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="service-hover text-center">
                            <i class="flaticon-timing-belt"></i>
                            <h4>Belts & Hoses</h4>
                            <p> Which of usundertakes laborious physical exercise, except to ut obtain some advantage from it.</p>
                            <a href="#" class="showhover">Read More</a>
                        </div><!-- end service-hover -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section db">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-left">
                            <h4>Schedule For Service</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="search-tab clearfix">
                            <div class="search-wrapper">
                                <form>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Name<sup>*</sup></label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Email<sup>*</sup></label>
                                            <input type="email" class="form-control" placeholder="">
                                        </div><!-- end col -->
                                    </div>

                                    <hr class="invis2">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Make / Model</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Service<sup>*</sup></label>
                                            <select name="orderby" class="selectpicker">
                                                <option>Wheel Works</option>
                                                <option>Select Dropdown 01</option>
                                                <option>Select Dropdown 02</option>
                                                <option>Select Dropdown 03</option>
                                                <option>Select Dropdown 04</option>
                                                <option>Select Dropdown 05</option>
                                            </select>
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label>Schedule Date & Time</label>
                                            <div class="inner-addon right-addon">
                                                <i class="glyphicon glyphicon-time"></i>
                                                <input type="text" class="form-control" placeholder="">
                                            </div>
                                        </div><!-- end col -->
                                    </div>

                                    <hr class="invis1">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <button class="btn btn-primary btn-block">REQUEST A SERVICE</button>
                                        </div><!-- end col -->

                                        <div class="col-md-8 col-sm-8 col-xs-12">
                                            <p><sup>*</sup> Our customercare excutive will contact you within <small>24 hours</small> now onwards.</p>
                                        </div><!-- end col -->
                                    </div>
                                </form><!-- end form -->
                            </div><!-- end search-wrapper -->
                        </div><!-- end search-tab -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Service Hours</h5>
                            <ul>
                                <li>Monday - Friday <span>09am - 18pm</span></li>
                                <li>Saturday <span>10am - 15pm</span></li>
                                <li>Sunday <span class="wc">Closed</span></li>
                            </ul>
                            <p><sup>*</sup> Every 3rd Staurday and all got holidays are closed.</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        CALLBOX
        ********************************************** -->
        <div class="section bg callmewrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <h2><i class="flaticon-customer-service"></i> emergeny Auto Service, Please Call our Toll free number!</h2>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <h2><i class="flaticon-phone-call"></i> (326) 148-557-2565</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        BRANDS
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-center">
                            <h4>Supported Brands</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row clients">
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_01.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_02.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_03.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_04.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_05.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_06.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_07.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_08.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_09.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-15 col-sm-3 col-xs-6">
                        <img src="uploads/client_10.jpg" alt="" class="img-responsive">
                    </div>
                </div><!-- end carousel -->
            </div><!-- end container -->
        </div><!-- end section -->

       

       


        

        
        </section>
        <!--/ sub apge ends -->
        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

    <script type="text/javascript">
        (function($) {
        "use strict";
        $('#myCarousel').carousel({
            interval: 4000
            });

            // handles the carousel thumbnails
            $('[id^=carousel-selector-]').click( function(){
              var id_selector = $(this).attr("id");
              var id = id_selector.substr(id_selector.length -1);
              id = parseInt(id);
              $('#myCarousel').carousel(id);
              $('[id^=carousel-selector-]').removeClass('selected');
              $(this).addClass('selected');
            });

            // when the carousel slides, auto update
            $('#myCarousel').on('slid', function (e) {
              var id = $('.item.active').data('slide-number');
              id = parseInt(id);
              $('[id^=carousel-selector-]').removeClass('selected');
              $('[id=carousel-selector-'+id+']').addClass('selected');
        });
        })(jQuery);
    </script>

</body>

</html>