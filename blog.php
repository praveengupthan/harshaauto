<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/grid-gallery.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">       
         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Blog</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li> 
                                    <li class="active">Blog</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <div class="section">
            <div class="container">
                <div class="row blog-list withimage">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="blog-wrapper">
                            <div class="post-media entry">
                                <img src="uploads/single_car_01.png" alt="" class="img-responsive">
                                <div class="magnifier colorized">
                                    <a href="ToyotaKirloskarMotorpartially.php"><i class="flaticon-link"></i></a>
                                </div>
                            </div><!-- end media -->
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="blog-description">
                            <h4><a href="ToyotaKirloskarMotorpartially.php">Toyota Kirloskar Motor partially resumes Retail & After-Sales Operations</a></h4>   
                            <ul class="list-inline hidden-xs">
                                <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
                                <!-- <li><a href="#"><i class="fa fa-eye"></i> 58 Views</a></li>
                                <li><a href="#"><i class="fa fa-comment-o"></i> 10 Comments</a></li> -->
                            </ul>
                            <p>New Delhi, May 12 (PTI) Toyota Kirloskar Motor (TKM) on Tuesday said its dealerships and aftersales operations have partially resumed after weeks of nationwide lockdown .....</p>
                            <a class="blogread" href="ToyotaKirloskarMotorpartially.php">Read More</a>
                        </div><!-- end blog-wrapper -->
                    </div><!-- end col --> 
                </div><!-- end row -->

               

              


               
              
                            
                <!-- <nav class="text-center">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </nav>     -->
            </div><!-- end container -->
        </div><!-- end section -->


        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

       

       
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <script src="js/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
    <?php include 'scripts.php' ?>
   

</body>

</html>