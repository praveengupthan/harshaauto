 <!-- ******************************************
        START HEADER 
        ********************************************** -->
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3 col-xs-12">
                        <div class="logo-wrapper clearfix">
                            <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""
                                    class="img-responsive"></a>
                        </div><!-- end logo -->
                    </div><!-- end col -->

                    <div class="col-md-10 col-sm-9 col-xs-12">
                        <div class="header-contact clearfix">
                            <p><i class="flaticon-pin alignleft"></i> 2-40/5, Old Bombay Highway,<br>Kothaguda, Hyderabad-500084
                            </p>
                        </div>
                        <div class="header-contact clearfix">
                            <p><i class="flaticon-icon-818 alignleft"></i> Mon - Sun 9.00 - 20.00,<br>Sunday Open</p>
                        </div><!-- end header-contact -->

                        <div class="header-contact clearfix">
                            <p><i class="flaticon-technology alignleft"></i> +91 40 39997999<br>Call us for enquiry</p>
                        </div><!-- end header-contact -->

                        <div class="hidden-xs header-search clearfix text-right">
                            <form class="search-form">
                                <div class="form-group has-feedback">
                                    <label for="search" class="sr-only">Search on this site..</label>
                                    <input type="text" class="form-control" name="search" id="search"
                                        placeholder="Search on this site..">
                                    <span class="fa fa-search form-control-feedback"></span>
                                </div>
                            </form>
                        </div><!-- end header-contact -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end header -->

        <div class="transparent-header">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                           
                            <li><a href="about.php">About us</a></li>
                            <li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">Group of Companies <span
                                        class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="harsha-hero.php">Sri Harsha Hero</a></li>
                                    <li><a href="harsha-toyota.php">Harsha Toyota</a></li>
                                    <li><a href="harsha-trucking.php">Sri Harsha Trucking</a></li>
                                    <li><a href="harsha-volvo.php">Sree Harsha Volvo</a></li>
                                    <li><a href="javascript:void(0)">Sri Viswaroopa Automotive </a></li>
                                </ul>
                            </li>
                            <li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">Mega Auto <span
                                        class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="newvehicles.php">New Vehicles</a></li>
                                    <li><a href="events.php">Events</a></li>
                                    <li><a href="quick-service-info.php">Quick Service Info</a></li>
                                    <li><a href="#">Latest Offers</a></li>
                                    <li><a href="javascript:void(0)">Festive Dhamaka </a></li>
                                </ul>
                            </li>
                            <li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"> Media <span
                                        class="fa fa-angle-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="gallery.php">Photo Gallery</a></li>
                                    <li><a href="videos.php">Videos</a></li>                                   
                                </ul>
                            </li>
                            <li class="dropdown hasmenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"> Updates <span class="fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="blog.php">Blog</a></li>
                                    <li><a href="javascript:void(0)">Latest News</a></li>                                   
                                </ul>
                            </li>
                            <li><a href="javascript:void(0)">Careers</a></li>                           
                            <li><a href="contact.php">Contact</a></li>
                            <li><a href="http://hrms.harshaauto.co.in/" target="_blank">HRMS</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="social-header"><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-header"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>                           
                            <li class="social-header"><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>                           
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
                <!--/.container-fluid -->
            </nav><!-- end nav -->
        </div><!-- end transparent header -->
