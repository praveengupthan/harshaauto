<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">       
         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Contact</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>                                   
                                    <li class="active">Contact</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="section-title clearfix">
                            <h5>Contact Details</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->

                        <div class="contact-departments contact-version clearfix">
                            <div class="contact-carousel owl-carousel owl-theme">
                                <div>
                                    <h5 class="custom-title">Sales Department</h5>
                                    <ul class="contact-widget clearfix">
                                        <li>
                                            <i class="fa fa-map-marker alignleft"></i> 
                                            <strong>Address:</strong>
                                            #2-40/5, Old Bombay Highway, Kothaguda, Hyderabad
                                        </li>
                                        <li>
                                            <i class="fa fa-envelope-o alignleft"></i> 
                                            <strong>Have any questions?</strong>
                                            info@harshaautogroup.com
                                        </li>
                                        <li>
                                            <i class="fa fa-phone alignleft"></i>
                                            <strong>Call us & Hire us</strong>
                                            040-4437-4437
                                        </li>                                       
                                    </ul>
                                </div>
                               
                            </div><!-- end carousel -->
                            <div class="contact-version social-icons">
                                <ul class="list-inline">
                                    <li><strong>We Are Social:</strong></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="section-title clearfix">
                            <h5>Send Message Us</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->

                        <div class="search-tab lightversion clearfix">
                            <h5 class="custom-title">I would like to discuss:</h5>
                            <div class="search-wrapper">
                                <form class="row">                                    

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Your Name *">
                                    </div><!-- end col -->

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" class="form-control" placeholder="Your Email *">
                                    </div><!-- end col -->

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Phone Number *">
                                    </div><!-- end col -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea class="form-control" placeholder="Your Message"></textarea>
                                    </div><!-- end col -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <button class="btn btn-primary btn-block">SUBMIT NOW</button>
                                    </div><!-- end col -->
                                </form>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        MAP
        ********************************************** -->
        

        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>