<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">

        <!-- banner-->
        <div class="banner-groupitem">
            <img src="uploads/toyota-banner.jpg" alt="" class="img-responsive">
        </div>
        <!--/ banner -->

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Harsha Toyota</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="javascript:void(0)">Group Companies</a></li>
                                    <li class="active">Harsha Toyota</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="post-media service-img">
                            <img src="uploads/toyota2.png" alt="" class="img-responsive">
                        </div><!-- end post-media -->                                  
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="section-title small-margin-title clearfix">
                            <h5>Welcome To Harsha Toyota</h5>
                            <hr class="custom">
                        </div><!-- end section-title -->            
                        <div class="service-text">                        
                            <p>The only dealer partner in India which won the confidence of Toyota and able to be the reliable channel partner for Toyota in three states of India i.e. Andhra Pradesh, Telangana & Tamil Nadu for sale and service of Toyota cars. Equipped with state of the art infrastructure facilities best in its class (all in one roof concept like sales, spares, service and body shop in one place), well-trained and committed manpower and through focus on customer satisfaction by delivering quality service. Placing customer satisfaction first, integrating sales with service and service parts in a single convenient location, we contribute to speedy and efficient service, allowing customers to experience the convenience and pleasure of owning Toyota automobile.</p> 
                        </div><!-- end service-text -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->
      

        <!-- ******************************************
        SERVICES SECTION
        ********************************************** -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix">
                            <h4>TOYOTA U TRUST</h4>
                            <p>Toyota Kirloskar Motors in India started the Toyota U Trust Program in 2007. Our main aim is to provide the used car buyer and sellers an experience at par with New Car buyer.</p>
                            <p>Toyota U Trust is set to facilitate Sales, Purchase & Exchange of Toyota and Non Toyota Cars. It allows potential customers an opportunity to exchange their old cars of any make for a brand new Toyota or a competitively priced used Toyota car.</p>
                            <p>Used car segment in the country is largely unorganized and with Toyota U Trust we want to ensure our customers gets necessary choice, convenience and transparency when they buy or sell a used car.</p>
                            <p>Each car is purchased at fair price and will go through extensive inspection and refurbishment process so that customers get the best quality cars.</p>
                            <p>When customer purchase Toyota used car at Toyota U Trust they are eligible to obtain certification and warranty offer backed by Toyota. Along with upto 2 Years/30,000 km warranty offer customer can get upto three labour free services at any Toyota dealerships.</p>
                           
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->


              

              
            </div><!-- end container -->         

        </div><!-- end section -->     
       



        <!-- ******************************************
        FORM SECTION
        ********************************************** -->
        <div class="section db">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title clearfix text-left">
                            <h4>Harsha Toyota Branches</h4>
                            <hr class="custom">
                        </div><!-- end section-title -->
                    </div><!-- end col -->
                </div><!-- end row -->

                <div class="row">                   
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Hyderabad</h5>                            
                            <p>D No. 2-40/5, Old Bombay Highway, Kothaguda, Hyderabad-500084</p>
                            <p>+91 40443 74437</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Nellore</h5>
                            <p>Harsha Automotive Pvt. Ltd., Survey No. 83, NH 5, Kakaturu Village, Venkatachalam(MD), Nellore-524320</p>
                            <p>+91 40443 74437</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Chennai</h5>
                            <p>No.65A, 1st Floor, Cauvery Tower, Anna Salai, Guindy, (Land Mark: Beside SBI-Guindy Branch) Chennai-600032</p>
                            <p>+91 95000 96573</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Tirupathi</h5>
                            <p>Door No. 8-103, Renigunta - Chittoor Bypass Road, Tukivakam(Post), Tirupati-517520</p>
                            <p>+91 40443 74437</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i> Kurnool</h5>
                            <p>Survey No. 233/1C, Besides Jubilee Petrol Pump, NH 7, Mamidalapadu(Post), Kurnool-518003</p>
                            <p>+91 40443 74437</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i>VELAPPANCHAVADI,  Chennai</h5>
                            <p>No. 142-A, Poonamallee High Road, NH 4, VELAPPANCHAVADI, (Landmark - Near ACS Medical College), Chennai-600077</p>
                            <p>+91 40443 74437</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i>Thiruvallur</h5>
                            <p>No. 5/6,  CV Naidu Salai, Thiruvallur-602001</p>
                            <p>+91 44 3999 7999</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-hours clearfix">
                            <h5><i class="fa fa-clock-o"></i>Chrompet, chennai</h5>
                            <p>No. 15, Rangaswamy Street, Chambers Colony, Behind Adyar Anand Bhavan, Chrompet, Chennai-600044</p>
                            <p>+91 44 3999 7999</p>
                        </div><!-- end service-hours -->
                    </div><!-- end col -->






                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        CALLBOX
        ********************************************** -->
        <div class="section bg callmewrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <h2><i class="flaticon-customer-service"></i> Please Call our Toll free number!</h2>
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <h2><i class="flaticon-phone-call"></i>1800 572 2888</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        BRANDS
        ********************************************** -->
      
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>