<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <?php include 'header.php'?>

        <!-- sub page start -->
        <section class="subpage">

         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Test Drive</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="#">Home</a></li>
                                    <li class="active">Test Drive</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->
        <div class="section" style="padding-top:0;">
            <!-- image -->
            <div class="booking-banner">
                <img src="uploads/slider_01.jpg" class="img-responsive" style="width:100%">
            </div>
            <!--/ image -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="">
                            <div class="row">                                

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 submit_form shipping_address">

                                <div class="section-title clearfix">
                                        <h5>Test Drive</h5>
                                        <hr class="custom">
                                    </div><!-- end section-title -->
                                    <form action="#" class="row">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter Name*</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter Name">
                                            </div>                                           
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter your vehicle model name *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter your vehicle model name">
                                            </div>                                           
                                        </div>    
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <span>Enter Date*</span>
                                                <input type="date" class="form-control select-drop" placeholder="Enter Date">
                                            </div>                                           
                                        </div><!-- end col -->   
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter Time *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter Time">
                                            </div>                                           
                                        </div>  
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter City *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter City">
                                            </div>                                           
                                        </div>    
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter Location *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter Location">
                                            </div>                                           
                                        </div>        
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter Phone no  *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter Phone no ">
                                            </div>                                           
                                        </div>      
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <span>Enter Mail ID   *</span>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter Mail ID  ">
                                            </div>                                           
                                        </div>    
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <button class="btn btn-primary">Submit</button>                                                  
                                        </div>                                               
                                    </form>
                                </div> <!-- /submit_form -->

                                                        
                            </div> <!-- /row -->
                        </div> <!-- /check_out_form -->

                      
                    </div>
                </div>
            </div><!-- end container -->
        </div><!-- end section -->
        </section>
        <!--/ sub apge ends -->
        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <?php include 'scripts.php' ?>

</body>

</html>