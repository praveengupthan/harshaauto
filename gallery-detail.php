<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Harsha Auto</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">  

    <link rel="stylesheet" type="text/css" href="revolution/settings.css">
    <link rel="stylesheet" type="text/css" href="revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/grid-gallery.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <?php include 'header.php'?>
        <!-- sub page start -->
        <section class="subpage">       
         <!-- ******************************************
        PAGE TITLE
        ********************************************** -->

        <div class="section page-title">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="title-area pull-left">
                            <h2>Awareness Toyota- Harsha Toyota</h2>
                        </div><!-- /.pull-right -->
                        <div class="pull-right">
                            <div class="bread">
                                <ol class="breadcrumb">
                                    <li><a href="index.php">Home</a></li> 
                                    <li><a href="javawscript:void(0)">Media</a></li>  
                                    <li><a href="gallery.php">Photo Gallery</a></li>                                                  
                                    <li class="active">Awareness Toyota- Harsha Toyota</li>
                                </ol>
                            </div><!-- end bread -->
                        </div><!-- /.pull-right -->
                    </div><!-- end col -->
                </div><!-- end page-title -->
            </div><!-- end container -->
        </div><!-- end section -->

        <!-- ******************************************
        PAGE WRAPPER
        ********************************************** -->

        <div class="container gallery-block grid-gallery">           
            <div class="row">
                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/1.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/2.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/3.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/4.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/5.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/6.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/7.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/8.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/9.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/10.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/11.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/12.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/13.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/14.jpeg">
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 col-sm-6 item">
                    <a class="lightbox" href="uploads/gallery/1.jpeg">
                        <img class="img-responsive image scale-on-hover" src="uploads/gallery/15.jpeg">
                    </a>
                </div>
               
            </div>
        </div>

       

       
        </section>
        <!--/ sub apge ends -->        
        <?php include 'footer.php'?>
        <div class="dmtop"><i class="fa fa-angle-up"></i></div>
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <script src="js/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
    <?php include 'scripts.php' ?>
   

</body>

</html>