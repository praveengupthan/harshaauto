<!-- ******************************************
        FOOTER
        ********************************************** -->
        <div class="section footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>About Harsha Auto</h4>
                            </div><!-- end widget-title -->
                            <div class="about-widget">
                                <p>A name of trust for many leading automobile brands like Toyota, Hero MotoCorp, Bharat Benz, Volvo etc.</p>
                                <a href="#" class="readmore">Know More</a>
                            </div><!-- end about-widget -->
                        </div><!-- end widget -->
                    </div><!-- end col -->                  
                </div><!-- end row -->

                <hr>

                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Get In Contact</h4>
                            </div><!-- end widget-title -->

                            <ul class="contact-widget clearfix">
                                <li><i class="fa fa-map-marker"></i> Address : D No. 2-40/5, Old Bombay Highway, Kothaguda, Hyderabad-500084</li>
                                <li><i class="fa fa-phone"></i> 91 40 39997999</li>
                                <li><i class="fa fa-envelope-o"></i> harshaauto@gmail.com</li>                               
                                <li><a href="#">Find Us On Map</a></li>
                            </ul>
                        </div><!-- end widget -->
                    </div><!-- end col -->                  

                    <div class="col-md-4 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Subscribe Us</h4>
                            </div><!-- end widget-title -->

                            <div class="footer-newsletter clearfix">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control input-lg" placeholder="Email Address...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-lg" type="button">
                                            Go
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Working Hours</h4>
                            </div><!-- end widget-title -->

                            <ul class="related-post working-hours clearfix">
                                <li>
                                    <h5>Sales &amp; Service Department:</h5>
                                    <p>Monday to Sunday: 09.00 am to 06.00 pm</p>                                   
                                </li>                               
                            </ul><!-- end related -->
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end footer -->

        <!-- ******************************************
        COPYRIGHTS
        ********************************************** -->
        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 text-left">
                        <p>Copyrights <small>© 2020</small> All Rights Reserved by <a
                                href="javascript:void(0)">Harsha Auto</a>.</p>
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <ul class="list-inline">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Policies</a></li>
                            <li><a href="#">Buy</a></li>
                            <li><a href="#">Sell</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->